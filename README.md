# Ansible Role: VSCodium

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-vscodium/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-vscodium/-/commits/main)

This role installs [VSCodium](https://vscodium.com/), an open source and Microsoft's telemetry free application on any supported host.

## TODO:

- Extensions versioning

## Why Does This Exist

I concern about privacy and I want an automatic way to install VSCodium on any machine.

Microsoft’s vscode source code is open source (MIT-licensed), but the product available for download (Visual Studio Code) is licensed under [this not-FLOSS license](https://code.visualstudio.com/license) and contains telemetry/tracking. According to [this comment](https://github.com/Microsoft/vscode/issues/60#issuecomment-161792005) from a Visual Studio Code maintainer:

> When we [Microsoft] build Visual Studio Code, we do exactly this. We clone the vscode repository, we lay down a customized product.json that has Microsoft specific functionality (telemetry, gallery, logo, etc.), and then produce a build that we release under our license.

When you clone and build from the vscode repo, none of these endpoints are configured in the default product.json. Therefore, you generate a “clean” build, without the Microsoft customizations, which is by default licensed under the MIT license.

The VSCodium project exists so that you don’t have to download+build from source. This project includes special build scripts that clone Microsoft’s vscode repo, run the build commands, and upload the resulting binaries for you to [GitHub releases](https://github.com/VSCodium/vscodium/releases). **These binaries are licensed under the MIT license. Telemetry is disabled.**

## Requirements

None

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-vscodium enmanuelmoreira.vscodium
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    vscodium_gpg_key: https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg

    vscodium_user: user
    vscodium_group: user_group

    vscodium_extensions_state: present
    vscodium_extensions: []

Specific distro vars are located into `vars` directory.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.vscodium

## License

MIT / BSD
